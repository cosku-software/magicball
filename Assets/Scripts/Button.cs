﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

    private Renderer ren;
    public GameObject door;
    private int colliderCount = 0;

    void Start()
    {
        ren = GetComponent<Renderer>();
    }
    void OnTriggerEnter(Collider other)
    {
        if (colliderCount == 0)
        {
            door.SetActive(false);
            ren.material.color = new Color(1, 0, 0);
            transform.localPosition += new Vector3(0, 0, 0.1f);
        }
        colliderCount++;
        Debug.Log(colliderCount);
    }

    void OnTriggerExit(Collider other)
    {
        colliderCount--;
        if (colliderCount == 0)
        {
            door.SetActive(true);
            ren.material.color = new Color(0, 1, 0);
            transform.localPosition -= new Vector3(0, 0, 0.1f);
        }
        Debug.Log(colliderCount);
    }
}
